﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ImageServe.Models
{
    public class Like
    {
        public Like()
        {
            DateLiked = DateTime.UtcNow;
        }

        [Required]
        public int ImageId { get; set; }
        public Image Image { get; set; }

        [Required]
        public string UserId { get; set; }
        public ImageServeUser User { get; set; }

        public DateTime DateLiked { get; set; }
    }
}
