﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ImageServe.Models
{
    public class GroupUser
    {
        public GroupUser()
        {
            DateJoined = DateTime.UtcNow;
        }
        [Required]
        public int GroupId { get; set; }
        public Group Group { get; set; }

        [Required]
        public string UserId { get; set; }
        public ImageServeUser User { get; set; }

        public DateTime DateJoined { get; set; }
    }
}
