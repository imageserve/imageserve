﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImageServe.Models
{
    public class ImageServeUser : IdentityUser
    {
        public ImageServeUser()
        {
            Images = new List<Image>();
            Comments = new List<Comment>();
            GroupsCreated = new List<Group>();
            GroupsJoined = new List<GroupUser>();
        }

        public string Avatar { get; set; }

        public List<Image> Images { get; set; }

        public List<Comment> Comments { get; set; }

        public List<Group> GroupsCreated { get; set; }

        public List<GroupUser> GroupsJoined { get; set; }

        public List<Like> Likes { get; set; }
    }
}
