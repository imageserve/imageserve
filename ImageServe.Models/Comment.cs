﻿using ImageServe.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ImageServe.Models
{
    public class Comment : BaseModel<int>
    {
        public Comment()
        {
            DateCommented = DateTime.UtcNow;
        }

        [Required]
        public string UserId { get; set; }
        public ImageServeUser User { get; set; }

        [Required]
        public int ImageId { get; set; }
        public Image Image { get; set; }

        public string Content { get; set; }

        public DateTime DateCommented { get; set; }
    }
}
