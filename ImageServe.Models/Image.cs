﻿using ImageServe.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ImageServe.Models
{
    public class Image : BaseModel<int>
    {
        public Image()
        {
            DateUploaded = DateTime.UtcNow;
            Tags = new List<ImageTag>();
            Likers = new List<Like>();
            Comments = new List<Comment>();
        }

        [Required]
        public string Url { get; set; }

        public string Description { get; set; }

        public DateTime DateUploaded { get; set; }

        [Required]
        public string UserId { get; set; }
        public ImageServeUser User { get; set; }

        public ICollection<ImageTag> Tags { get; set; }

        public ICollection<Like> Likers { get; set; }

        public ICollection<Comment> Comments { get; set; }

        public int GroupId { get; set; }
        public Group Group { get; set; }
    }
}
