﻿using ImageServe.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ImageServe.Models
{
    public class Group : BaseModel<int>
    {
        public Group()
        {
            Users = new List<GroupUser>();
            Images = new List<Image>();
            DateCreated = DateTime.UtcNow;
            
            
        }
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public string AdminId { get; set; }
        public ImageServeUser Admin { get; set; }

        public ICollection<GroupUser> Users { get; set; }

        public ICollection<Image> Images { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
