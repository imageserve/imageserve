﻿using System;
using System.Collections.Generic;
using System.Text;
using ImageServe.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ImageServe.Data
{
    public class ImageServeDbContext : IdentityDbContext<ImageServeUser>
    {
        public DbSet<Image> Images { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Group> Groups { get; set; }

        public DbSet<ImageTag> ImageTags { get; set; }

        public DbSet<Like> Likes { get; set; }

        public DbSet<GroupUser> GroupUsers { get; set; }


        public ImageServeDbContext(DbContextOptions<ImageServeDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            base.OnConfiguring(builder);

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);


            //one to many relations
            builder.Entity<Image>()
                .HasOne(i => i.User)
                .WithMany(u => u.Images)
                .HasForeignKey(i => i.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Image>()
                .HasOne(i => i.Group)
                .WithMany(g => g.Images)
                .HasForeignKey(i => i.GroupId);




            builder.Entity<Comment>()
                .HasOne(c => c.Image)
                .WithMany(i => i.Comments)
                .HasForeignKey(c => c.ImageId);

            builder.Entity<Comment>()
                .HasOne(c => c.User)
                .WithMany(u => u.Comments)
                .HasForeignKey(c => c.UserId)
                .OnDelete(DeleteBehavior.Restrict);




            builder.Entity<Group>()
                .HasOne(g => g.Admin)
                .WithMany(u => u.GroupsCreated)
                .HasForeignKey(g => g.AdminId)
                .OnDelete(DeleteBehavior.Restrict);




            //many to many relations
            builder.Entity<Like>()
                .HasKey(l => new { l.UserId, l.ImageId });

            builder.Entity<Like>()
                .HasOne(l => l.Image)
                .WithMany(i => i.Likers)
                .HasForeignKey(l => l.ImageId);

            builder.Entity<Like>()
                .HasOne(l => l.User)
                .WithMany(u => u.Likes)
                .HasForeignKey(l => l.UserId);




            builder.Entity<ImageTag>()
                .HasKey(it => new { it.TagId, it.ImageId });

            builder.Entity<ImageTag>()
                .HasOne(it => it.Image)
                .WithMany(i => i.Tags)
                .HasForeignKey(it => it.ImageId);

            builder.Entity<ImageTag>()
                .HasOne(it => it.Tag)
                .WithMany(t => t.Images)
                .HasForeignKey(it => it.TagId);




            builder.Entity<GroupUser>()
                .HasKey(gu => new { gu.UserId, gu.GroupId });

            builder.Entity<GroupUser>()
                .HasOne(gu => gu.User)
                .WithMany(u => u.GroupsJoined)
                .HasForeignKey(gu => gu.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<GroupUser>()
                .HasOne(gu => gu.Group)
                .WithMany(g => g.Users)
                .HasForeignKey(gu => gu.GroupId);
        }
    }
}
